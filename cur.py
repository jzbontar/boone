"""
See CUR matrix decompositions for improved data analysis by
M. Mahoney and P. Drineas in PNAS, 2009.
"""

import numpy as np
import scipy.sparse.linalg as sla


def column_select(A, UsV, k, err):
    """Importance sampling."""
    U, s, V = UsV
    pi = leverage_scores(V)
    c = k*np.log(k)/err**2
    ib = [i for i in xrange(A.shape[1]) if np.random.rand() < min(1, c*pi[i])]
    C = A[:, ib]
    return C, pi, ib


def leverage_scores(V):
    """Compute normalized statistical leverage scores."""
    n, k = V.shape
    pi = [1./k*np.sum(V[i, :]**2) for i in xrange(n)]
    return pi


def cur(A, k, err):
    """Main algorithm for computing CUR matrix decomposition."""
    U, s, V = sla.svds(A, k)
    V = V.T
    U = U.T
    C, leverage_cols, ib_cols = column_select(A, [U, s, V], k, err)
    UsV = [V, s, U.T]
    R, leverage_rows, ib_rows = column_select(A.T, UsV, k, err)
    R = R.T
    U = np.dot(np.dot(np.linalg.pinv(C), A), np.linalg.pinv(R))
    return C, U, R, (leverage_cols, leverage_rows), (ib_cols, ib_rows)


if __name__ == '__main__':
    from scipy.sparse import csr_matrix

    A = np.random.rand(60,6000)
    k = 6
    err = 1
    C, U, R, leverage, ib = cur(A, k, err)
    A_hat = np.dot(C, np.dot(U, R))
    print "Fro. error (CUR): %5.4f" % np.linalg.norm(A-A_hat, 'fro')

    U, s, V = sla.svds(A, k)
    S = np.diag(s)
    A_k = np.dot(U, np.dot(S, V))
    print "Fro. error (optimal SVD): %5.4f" % np.linalg.norm(A-A_k, 'fro')
