#! /usr/bin/env python2

import gzip
import time
import os
import sys

import numpy as np
from xlrd import open_workbook

import download

color = {
    'metabolism/mitochondria': '#8b42a5',
    'PEROXISOME': '',
    'amino acid biosynth&transport/nitrogen utilization': '#3b4ca8',
    'SECRETION': '',
    'protein folding/protein glycosylation/cell wall biogenesis&integrity': '#eb1f28',
    'cell polarity/morphogenesis': '#f48092',
    'DNA replication/repair/HR/cohesion': '#79cd67',
    'chromosome segregation/kinetochore/spindle/microtubule': '#22632e',
    'protein degradation/proteosome': '#674522',
    'nuclear-cytoplasic transport': '#ef2d21',
    'chromatin/transcription': '#7ad2db',
    'RNA processing': '#226365',
    'ribosome/translation': '#a71c23',
}

sheet = open_workbook('data/bioprocess_annotations_costanzo2009.xls').sheets()[0]
f = open('data/bioprocess.txt', 'w')
f.write('gene\tcolor\n')
for i in range(sheet.nrows):
    gene = sheet.cell(i, 1).value.encode()
    processes = sheet.cell(i, 2).value.encode().split(';')
    f.write('{}\t{}\n'.format(gene, color.get(processes[0], '')))

array_orfs = [tuple(line.split()) for line in open('data/sgadata_costanzo2009_array_list.txt')]
query_orfs = [tuple(line.split()) for line in open('data/sgadata_costanzo2009_query_list_101120.txt')]
array2col = {gene[0]: i for i, gene in enumerate(array_orfs)}
query2row = {gene[0]: i for i, gene in enumerate(query_orfs)}

if not os.path.isfile('data/X.npy'):
    X = np.zeros((len(query_orfs), len(array_orfs)))
    for line in gzip.open('data/sgadata_costanzo2009_rawdata_101120.txt.gz'):
        query_orf, _, array_orf, _, interaction, _, _, _, _, _, _, _, _ = line.split()
        if not np.isnan(float(interaction)):
            X[query2row[query_orf], array2col[array_orf]] = float(interaction)
    np.save('data/X', X)
X = np.load('data/X.npy')

def cor(X):
    sigma = X.std(axis=0)
    Xzm = X - X.mean(axis=0)
    return np.dot(Xzm.T, Xzm) / np.maximum(1e-7, X.shape[0] * np.outer(sigma, sigma))

P = cor(X.T)
Pa = cor(X)

query_orfs_set = set(query_orfs)
for i in range(Pa.shape[0]):
    for j in range(i):
        x = array_orfs[i]
        y = array_orfs[j]
        if x in query_orfs_set and y in query_orfs_set:
            ii = query2row[x[0]]
            jj = query2row[y[0]]
            Pa[i,j] = Pa[j,i] = (Pa[i,j] + P[ii,jj]) / 2

removed = {line.split()[0] for line in open('data/80_removed_ORFs_Science.txt')}

f = open('data/boone.sif', 'w')
for i in range(Pa.shape[0]):
    for j in range(i):
        x = array_orfs[i]
        y = array_orfs[j]
        if abs(Pa[i,j]) > 0.2 and x[0] not in removed and y[0] not in removed:
            f.write('{} c {}\n'.format(x[1], y[1]))
