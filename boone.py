#! /usr/bin/env python2

import collections
import random
import time
import sys
import gzip
import os

from scipy.sparse import csr_matrix
from xlrd import open_workbook
import numpy as np

import cur

# parameters
RANDOM_SEED = 42
NUM_QUERY_GENES = 500
CUR_K = 100
CUR_ERR = 1
CUTOFF = 'intermediate'

random.seed(RANDOM_SEED)
np.random.seed(RANDOM_SEED)

assert(CUTOFF in {'lenient', 'intermediate', 'stringent'})

bioprocess_name = {
    'metabolism/mitochondria': 'Mitochondria',
    '1': 'Peroxisome', # ???
    'amino acid biosynth&transport/nitrogen utilization': 'Metabolism & amino acid biosynthesis', # ???
    '2': 'Secretion & vesicle transport', # ???
    'protein folding/protein glycosylation/cell wall biogenesis&integrity': 'Protein folding & glycosylation Cell wall biosynthesis',
    'cell polarity/morphogenesis': 'Cell polarity & morphogenesis',
    'DNA replication/repair/HR/cohesion': 'DNA replication & repair',
    'chromosome segregation/kinetochore/spindle/microtubule': 'Mitosis & chr.  segregation', # ???
    'protein degradation/proteosome': 'Nuclear migration & protein degradation', # ???
    'nuclear-cytoplasic transport': 'Nuclear-cytoplasmic transport',
    'chromatin/transcription': 'Chromatin & transcription',
    'RNA processing': 'RNA processing',
    'ribosome/translation': 'Ribosome & translation',
}

sheet = open_workbook('data/bioprocess_annotations_costanzo2009.xls').sheets()[0]
bioprocesses = collections.defaultdict(list)
for i in range(sheet.nrows):
    gene = sheet.cell(i, 0).value.encode()
    processes = [bioprocess_name[s] for s in sheet.cell(i, 2).value.encode().split(';') if s in bioprocess_name]
    bioprocesses[gene] = processes

array_orfs = [tuple(line.split()) for line in open('data/sgadata_costanzo2009_array_list.txt')]
query_orfs = [tuple(line.split()) for line in open('data/sgadata_costanzo2009_query_list_101120.txt')]

# select the query genes that have a bioprocess from the paper
query_orfs = [gene for gene in query_orfs if bioprocesses[gene[0]]]

# randomly sample genes
query_orfs = random.sample(query_orfs, NUM_QUERY_GENES)

# row and col mapping
array2col = {gene[0]: i for i, gene in enumerate(array_orfs)}
query2row = {gene[0]: i for i, gene in enumerate(query_orfs)}

# construct X
X = np.zeros((len(query_orfs), len(array_orfs)))
for line in gzip.open('data/sgadata_costanzo2009_{}Cutoff_101120.txt.gz'.format(CUTOFF)):
    query_orf, _, array_orf, _, interaction, _, _ = line.split()
    if query_orf in query2row and array_orf in array2col:
        X[query2row[query_orf], array2col[array_orf]] = float(interaction)

# CUR decomposition
C, U, R, _, (ib_cols, _) = cur.cur(X, CUR_K, CUR_ERR)

# write tab file
cols = [array_orfs[col][0] for col in ib_cols]
f = open('data/boone.tab', 'w')
f.write('\t'.join(cols) + '\tQuery ORF\tQuery gene name\n')
f.write('\t'.join('c' * len(cols)) + '\td\td\n')
f.write('\t' * len(cols) + 'meta\tmeta\n')
for i in range(NUM_QUERY_GENES):
    for j in range(len(cols)):
        f.write(repr(C[i,j]) + '\t')
    f.write('{}\t{}\n'.format(query_orfs[i][0], query_orfs[i][1]))
