#! /usr/bin/python2

import gzip
import time
import os
import sys
import cur

import numpy as np
from xlrd import open_workbook

np.random.seed(42)

groups = {
    'Golgi/endosome/vacuole/sorting': '#ff0000',
    'cell polarity/morphogenesis': '#00ff00',
    'protein folding/protein glycosylation/cell wall biogenesis&integrity': '#0000ff',
    'protein degradation/proteosome': '#ffff00',
    'ER<->Golgi traffic': '#ff00ff',
    'signaling/stress response': '#00ffff',
    'amino acid biosynth&transport/nitrogen utilization': '#770000',
}

gene2group = {}
sheet = open_workbook('data/bioprocess_annotations_costanzo2009.xls').sheets()[0]
for i in range(sheet.nrows):
    gene = sheet.cell(i, 1).value.encode()
    process = sheet.cell(i, 2).value.encode().split(';')[0]
    if process in groups:
        gene2group[gene] = process

A = [tuple(line.split()) for line in open('data/sgadata_costanzo2009_array_list.txt')]
Q = [tuple(line.split()) for line in open('data/sgadata_costanzo2009_query_list_101120.txt')]

Q = [q for q in Q if q[1] in gene2group]

Ai = {gene[0]: i for i, gene in enumerate(A)}
Qi = {gene[0]: i for i, gene in enumerate(Q)}

if 0:
    X = np.zeros((len(Q), len(A)))
    for line in gzip.open('data/sgadata_costanzo2009_rawdata_101120.txt.gz'):
        q, _, a, _, interaction, _, _, _, _, _, _, _, _ = line.split()
        if q in Qi and a in Ai and not np.isnan(float(interaction)):
            X[Qi[q], Ai[a]] = float(interaction)
    np.save('data/X', X)
X = np.load('data/X.npy')

# CUR decomposition
C, U, R, _, (ib_cols, _) = cur.cur(X, 50, 1)

# Orange
cols = [A[col][1] for col in ib_cols]
f = open('data/boone.tab', 'w')
f.write('\t'.join(cols) + '\tQuery ORF\tQuery gene name\tBiological process\n')
f.write('\t'.join('c' * len(cols)) + '\tstring\tstring\td\n')
f.write('\t' * len(cols) + 'meta\tmeta\tmeta\n')
for i in range(C.shape[0]):
    for j in range(C.shape[1]):
        f.write('{:.4f}\t'.format(C[i,j]))
    f.write('{}\t{}\t{}\n'.format(Q[i][0], Q[i][1], gene2group[Q[i][1]]))

# Cytoscape
def cor(X):
    sigma = X.std(axis=0)
    Xzm = X - X.mean(axis=0)
    return np.dot(Xzm.T, Xzm) / np.maximum(1e-7, X.shape[0] * np.outer(sigma, sigma))

QQ = cor(C.T)

f = open('data/boone_small.sif', 'w')
for i in range(QQ.shape[0]):
    for j in range(i):
        if abs(QQ[i,j]) > 0.35:
            f.write('{} c {}\n'.format(Q[i][1], Q[j][1]))

f = open('data/bioprocess_small.txt', 'w')
f.write('gene\tcolor\n')
for gene, group in gene2group.items():
    f.write('{}\t{}\n'.format(gene, groups[group]))
